package com.vojkan.calculatorapp.functions;

import java.math.BigDecimal;
import java.math.MathContext;

public class ScientificFunctions {

    private BigDecimal degreesToRadians(BigDecimal degrees) {
        return new BigDecimal(Math.toRadians(degrees.doubleValue()), MathContext.DECIMAL64);
    }

    private BigDecimal radiansToDegrees(double radians) {
        return new BigDecimal(Math.toDegrees(radians), MathContext.DECIMAL64);
    }

    public BigDecimal functionSquare(BigDecimal currentValue) {
        try {
            return currentValue.pow(2, MathContext.DECIMAL64);
        } catch (ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionCube(BigDecimal currentValue) {
        try {
            return currentValue.pow(3, MathContext.DECIMAL64);
        } catch (ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionEPowerX(BigDecimal currentValue) {
        try {
            return new BigDecimal(Math.pow(Math.E, currentValue.doubleValue()), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal function10PowerX(BigDecimal currentValue) {
        try {
            return new BigDecimal(Math.pow(10, currentValue.doubleValue()), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal function2PowerX(BigDecimal currentValue) {
        try {
            return new BigDecimal(Math.pow(2, currentValue.doubleValue()), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionReciprocialValue(BigDecimal currentValue) {
        try {
            return new BigDecimal(1).divide(currentValue, MathContext.DECIMAL64);
        } catch (ArithmeticException | NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionSquareRoot(BigDecimal currentValue) {
        try {
            return new BigDecimal(Math.sqrt(currentValue.doubleValue()), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionCubeRoot(BigDecimal currentValue) {
        try {
            return new BigDecimal(Math.cbrt(currentValue.doubleValue()), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionNaturalLogarithm(BigDecimal currentValue) {
        try {
            return new BigDecimal(Math.log(currentValue.doubleValue()), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionLogarithmBase10(BigDecimal currentValue) {
        try {
            return new BigDecimal(Math.log10(currentValue.doubleValue()), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionLogarithmBase2(BigDecimal currentValue) {
        try {
            BigDecimal firstValue = new BigDecimal(Math.log(currentValue.doubleValue()), MathContext.DECIMAL64);
            BigDecimal secondValue = new BigDecimal(Math.log(2), MathContext.DECIMAL64);
            return firstValue.divide(secondValue, MathContext.DECIMAL64);
        } catch (NumberFormatException | NullPointerException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionFactorial(BigDecimal currentValue) {
        try {
            BigDecimal fact = BigDecimal.ONE;
            if (currentValue.doubleValue() < 1) {
                throw new IllegalArgumentException(String.format("Illegal value, value = %f", currentValue.doubleValue()));
            }
            for (int i = 1; i <= currentValue.intValue(); i++) {
                fact = fact.multiply(BigDecimal.valueOf(i), MathContext.DECIMAL64);
            }
            return fact;
        } catch (NumberFormatException | NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionSine(BigDecimal currentValue, boolean radiansActivated) {
        try {
            return !radiansActivated ? new BigDecimal(Math.sin(degreesToRadians(currentValue).doubleValue()), MathContext.DECIMAL64)
                    : new BigDecimal(Math.sin(currentValue.doubleValue()), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionCosine(BigDecimal currentValue, boolean radiansActivated) {
        try {
            return !radiansActivated ? new BigDecimal(Math.cos(degreesToRadians(currentValue).doubleValue()), MathContext.DECIMAL64)
                    : new BigDecimal(Math.cos(currentValue.doubleValue()), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionTangent(BigDecimal currentValue, boolean radiansActivated) {
        try {
            return !radiansActivated ? new BigDecimal(Math.tan(degreesToRadians(currentValue).doubleValue()), MathContext.DECIMAL64)
                    : new BigDecimal(Math.tan(currentValue.doubleValue()), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionArcSine(BigDecimal currentValue, boolean radiansActivated) {
        try {
            return !radiansActivated ? radiansToDegrees(Math.asin(currentValue.doubleValue()))
                    : new BigDecimal(Math.asin(currentValue.doubleValue()), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionArcCosine(BigDecimal currentValue, boolean radiansActivated) {
        try {
            return !radiansActivated ? radiansToDegrees(Math.acos(currentValue.doubleValue()))
                    : new BigDecimal(Math.acos(currentValue.doubleValue()), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionArcTangent(BigDecimal currentValue, boolean radiansActivated) {
        try {
            return !radiansActivated ? radiansToDegrees(Math.atan(currentValue.doubleValue()))
                    : new BigDecimal(Math.atan(currentValue.doubleValue()), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionHyperbolicSine(BigDecimal currentValue) {
        try {
            return new BigDecimal(Math.sinh(currentValue.doubleValue()), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionHyperbolicCosine(BigDecimal currentValue) {
        try {
            return new BigDecimal(Math.cosh(currentValue.doubleValue()), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionHyperbolicTangent(BigDecimal currentValue) {
        try {
            return new BigDecimal(Math.tanh(currentValue.doubleValue()), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionInverseHyperbolicSine(BigDecimal currentValue) {
        try {
            double val = currentValue.doubleValue() + Math.sqrt(Math.pow(currentValue.doubleValue(), 2) + 1);
            return new BigDecimal(Math.log(val), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionInverseHyperbolicCosine(BigDecimal currentValue) {
        try {
            double val = currentValue.doubleValue() + Math.sqrt(Math.pow(currentValue.doubleValue(), 2) - 1);
            return new BigDecimal(Math.log(val), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionInverseHyperbolicTangent(BigDecimal currentValue) {
        try {
            double val = (1 + currentValue.doubleValue()) / (1 - currentValue.doubleValue());
            return new BigDecimal(Math.log(val)).multiply(BigDecimal.valueOf(0.5), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal functionRandom() {
        try {
            return new BigDecimal(Math.random(), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

}
