package com.vojkan.calculatorapp.functions;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class DisplayText {

    public String displayInsertedValue(StringBuilder sb, String inputText, String number) {
        if (inputText != null) {
            if (inputText.equals("0")) {
                if (Integer.parseInt(number) != 0) {
                    sb.append(number);
                } else {
                    sb.append(inputText);
                }
            } else if (inputText.equals("-0") && number != null) {
                    sb.append(number);
                    sb.insert(0, "-");
            } else {
                sb.append(inputText);
                sb.append(number);
            }
        }
        return sb.toString();
    }

    public String addCommaToValue(StringBuilder sb, BigDecimal value) {
        if (sb.length() != 0) {
            if (sb.toString().equals("-0")) {
                sb.deleteCharAt(sb.length() - 1);
            } else {
                sb.delete(0, sb.length());
            }
        }
        sb.append(value.stripTrailingZeros().toPlainString());
        if (value.precision() - value.scale() >= 4) {
            int br = 0;
            for (int i = value.toBigInteger().toString().length() - 1; i >= 0; i--) {
                br++;
                if (br == 3 && i != 0 && value.toBigInteger().toString().charAt(i - 1) != '-') {
                    sb.insert(i, ",");
                    i -= 2;
                    br = 2;
                }
            }
        }
        return sb.toString();
    }

    public String scientificNotation(StringBuilder sb, BigDecimal value) {
        if (sb.length() != 0) {
            sb.delete(0, sb.length());
        }
        double coefficient;
        int exponent = 0;
        try {
            if (value.compareTo(BigDecimal.ZERO) > 0) {
                if (value.doubleValue() > 10) {
                    while (value.doubleValue() > 10) {
                        value = value.divide(BigDecimal.valueOf(10), MathContext.DECIMAL64);
                        exponent++;
                    }
                    coefficient = value.setScale(6, RoundingMode.HALF_EVEN).doubleValue();
                    sb.append(coefficient).append("e+").append(exponent);
                } else if (value.doubleValue() < 1) {
                    while (value.doubleValue() < 1) {
                        value = value.multiply(BigDecimal.valueOf(10), MathContext.DECIMAL64);
                        exponent++;
                    }
                    coefficient = value.setScale(6, RoundingMode.HALF_EVEN).doubleValue();
                    sb.append(coefficient).append("e-").append(exponent);
                }
            } else if (value.compareTo(BigDecimal.ZERO) < 0) {
                if (value.doubleValue() < -10) {
                    while (value.doubleValue() < -10) {
                        value = value.divide(BigDecimal.valueOf(10), MathContext.DECIMAL64);
                        exponent++;
                    }
                    coefficient = value.setScale(6, RoundingMode.HALF_EVEN).doubleValue();
                    sb.append(coefficient).append("e+").append(exponent);
                } else if (value.doubleValue() > -1) {
                    while (value.doubleValue() > -1) {
                        value = value.multiply(BigDecimal.valueOf(10), MathContext.DECIMAL64);
                        exponent++;
                    }
                    coefficient = value.setScale(6, RoundingMode.HALF_EVEN).doubleValue();
                    sb.append(coefficient).append("e-").append(exponent);
                }
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
