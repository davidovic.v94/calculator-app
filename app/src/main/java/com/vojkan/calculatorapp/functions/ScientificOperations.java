package com.vojkan.calculatorapp.functions;

import java.math.BigDecimal;
import java.math.MathContext;

public class ScientificOperations {

    public BigDecimal operationXPowerOfY(BigDecimal x, BigDecimal y) {
        try {
            if (x.compareTo(BigDecimal.ZERO) == 0  && y.compareTo(BigDecimal.ZERO) == 0) {
                return BigDecimal.ZERO;
            }
            return new BigDecimal(Math.pow(x.doubleValue(), y.doubleValue()), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal operationYPowerOfX(BigDecimal x, BigDecimal y) {
        try {
            if (x.compareTo(BigDecimal.ZERO) == 0  && y.compareTo(BigDecimal.ZERO) == 0) {
                return BigDecimal.ZERO;
            }
            return new BigDecimal(Math.pow(y.doubleValue(), x.doubleValue()), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal operationYRootOfX(BigDecimal x, BigDecimal y) {
        try {
            return new BigDecimal(Math.pow(x.doubleValue(), 1 / y.doubleValue()), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal operationLogarithmBaseY(BigDecimal x, BigDecimal y) {
        try {
            BigDecimal firstValue = new BigDecimal(Math.log(x.doubleValue()), MathContext.DECIMAL64);
            BigDecimal secondValue = new BigDecimal(Math.log(y.doubleValue()), MathContext.DECIMAL64);
            return firstValue.divide(secondValue, MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException | NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }

    public BigDecimal operationScaleByPowerOf10(BigDecimal x, BigDecimal y) {
        try {
            return x.multiply(BigDecimal.valueOf(Math.pow(10, y.doubleValue())), MathContext.DECIMAL64);
        } catch (NumberFormatException | ArithmeticException | NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }

}
