package com.vojkan.calculatorapp.activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import com.vojkan.calculatorapp.R;
import com.vojkan.calculatorapp.functions.DisplayText;
import com.vojkan.calculatorapp.functions.ScientificFunctions;
import com.vojkan.calculatorapp.functions.ScientificOperations;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class MainActivity extends AppCompatActivity {

    private TextInputLayout textResult;
    private TextView textRadian;
    private MaterialButton buttonClear, buttonAllClear, buttonArcSine, buttonArcCosine, buttonArcTangent, buttonSine, buttonCosine,
            buttonTangent, buttonInverseHyperbolicSine, buttonInverseHyperbolicCosine, buttonInverseHyperbolicTangent,
            buttonHyperbolicSine, buttonHyperbolicCosine, buttonHyperbolicTangent, buttonNaturalLog, buttonLogBase10, buttonLogBase2,
            buttonLogBaseY, buttonEPowerX, button10PowerX, button2PowerX, buttonYPowerX, buttonSecond, buttonXPowerY, buttonXRootY,
            buttonEngineeringExponent, buttonOpenBracket, buttonClosedBracket, buttonMemoryClear, buttonMemoryPlus, buttonMemoryMinus,
            buttonMemoryRecall, buttonSquare, buttonCube, buttonReciprocalValue, buttonSquareRoot, buttonCubeRoot, buttonRadians,
            buttonDegrees, buttonFactorial, buttonEulerNumber, buttonPi, buttonRandom, button0, button1, button2, button3, button4,
            button5, button6, button7, button8, button9, buttonDot, buttonEquals, buttonPlus, buttonMinus, buttonMultiply, buttonDivide,
            buttonPercent, buttonNegative;
    private Button buttonOperationPerformed;
    private Button buttonScientificOperationPerformed;

    private final Map<String, Integer> operationPriority = new HashMap<>();
    private Integer currentOperationPriority = null;
    private Integer lastOperationPriority = null;
    private final StringBuilder sb = new StringBuilder();

    private boolean numberSelected = false;
    private boolean operationPerformed = false;
    private boolean functionPerformed = false;
    private boolean percentPerformed = false;
    private boolean constantPerformed = false;
    private boolean negativePerformed = false;
    private boolean memoryPerformed = false;
    private boolean dotPerformed = false;
    private boolean firstTime = true;
    private boolean firstTimeMemory = true;
    private boolean memoryClear = true;
    private boolean radiansActivated = false;
    private boolean clearActivated = false;
    private boolean buttonSecondPerformed = false;
    private boolean maxReached = false;
    private boolean scientificMode = false;

    private final Stack<String> stackOperation = new Stack<>();
    private final Stack<BigDecimal> stackOperands = new Stack<>();
    private double[] stackOperandsArray;
    private ArrayList<String> stackOperationArray;

    private BigDecimal newValue = null;
    private BigDecimal percent = null;
    private BigDecimal negative = null;
    private BigDecimal firstOperand = null;
    private BigDecimal secondOperand = null;
    private BigDecimal result = null;
    private BigDecimal memoryValue = BigDecimal.ZERO;
    private float textSizePixel = 0;
    private float scaledDensity = 0;

    private String pendingOperation = "=";
    private String lastOperation = null;

    private ScientificFunctions scientificFunctions;
    private ScientificOperations scientificOperations;
    private DisplayText displayText;

    private static final String STATE_STRING_BUILDER = "StateStringBuilder";
    private static final String STATE_BUTTON_SECOND_PERFORMED = "StateButtonSecondPerformed";
    private static final String STATE_CONSTANT_PERFORMED = "StateConstantPerformed";
    private static final String STATE_MEMORY_VALUE = "StateMemoryValue";
    private static final String STATE_PERCENT_PERFORMED = "StatePercentPerformed";
    private static final String STATE_RADIANS_ACTIVATED = "StateRadiansActivated";
    private static final String STATE_MEMORY_CLEAR = "StateMemoryClear";
    private static final String STATE_MEMORY_FIRST_TIME = "StateMemoryFirstTime";
    private static final String STATE_MEMORY_PERFORMED = "StateMemoryPerformed";
    private static final String STATE_RESULT = "StateResult";
    private static final String STATE_PENDING_OPERATION = "PendingOperation";
    private static final String STATE_LAST_OPERATION = "LastOperation";
    private static final String STATE_TEXT_RESULT = "StateTextResult";
    private static final String STATE_NUMBER_SELECTED = "StateNumberSelected";
    private static final String STATE_MAXIMUM_REACHED = "StateMaximumReached";
    private static final String STATE_CLEAR_ACTIVATED = "StateClearActivated";
    private static final String STATE_OPERATION_PERFORMED = "StateOperationPerformed";
    private static final String STATE_FUNCTION_PERFORMED = "StateFunctionPerformed";
    private static final String STATE_NEGATIVE_PERFORMED = "StateNegativePerformed";
    private static final String STATE_DOT_PERFORMED = "StateDotPerformed";
    private static final String STATE_FIRST_TIME = "StateFirstTime";
    private static final String STATE_BUTTON_OPERATION_PERFORMED = "StateButtonOperationPerformed";
    private static final String STATE_BUTTON_SCIENTIFIC_OPERATION_PERFORMED = "StateButtonScientificOperationPerformed";

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        scaledDensity = getApplicationContext().getResources().getDisplayMetrics().scaledDensity;

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            if (scientificFunctions == null) {
                scientificFunctions = new ScientificFunctions();
            }
            if (scientificOperations == null) {
                scientificOperations = new ScientificOperations();
            }
            initializeScientificButtons();
            scientificMode = true;
        }

        initializeStandardButtons();
        initializeOperationPriorities();

        if (displayText == null) {
            displayText = new DisplayText();
        }

        if (textResult.getEditText() != null) {
            textResult.getEditText().setText("0");
            textSizePixel = textResult.getEditText().getTextSize();
            textResult.getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (textResult.getEditText() != null && !scientificMode) {
                        if (newValue != null && newValue.precision() == 1) {
                            textResult.getEditText().setTextSize(TypedValue.COMPLEX_UNIT_SP, textSizePixel / scaledDensity);
                        } else if (newValue != null && newValue.precision() == 10) {
                            textResult.getEditText().setTextSize(TypedValue.COMPLEX_UNIT_SP, textSizePixel / scaledDensity - 12);
                        } else if (newValue != null && newValue.precision() == 13) {
                            textResult.getEditText().setTextSize(TypedValue.COMPLEX_UNIT_SP, textSizePixel / scaledDensity - 20);
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }

        View.OnClickListener numberListener = v -> {
            Button b = (Button) v;

            if (constantPerformed || maxReached) {
                return;
            }

            if (sb.length() != 0) {
                sb.delete(0, sb.length());
            }

            if (operationPerformed || memoryPerformed || functionPerformed || percentPerformed) {
                if (pendingOperation.equals("=")) {
                    stackOperands.clear();
                    numberSelected = false;
                    result = null;
                }
                if (negativePerformed) {
                    negativePerformed = false;
                    if (stackOperands.isEmpty()) {
                        if (percentPerformed) {
                            textResult.getEditText().setText("0");
                        } else {
                            textResult.getEditText().setText("-0");
                        }
                    } else if (stackOperands.peek().doubleValue() != 0) {
                        textResult.getEditText().setText("");
                    }
                } else if (dotPerformed) {
                    dotPerformed = false;
                } else if (percentPerformed) {
                    percentPerformed = false;
                    textResult.getEditText().setText("");
                } else {
                    textResult.getEditText().setText("");
                }

                if (functionPerformed) {
                    functionPerformed = false;
                }

                if (operationPerformed) {
                    operationPerformed = false;
                }

                if (memoryPerformed) {
                    memoryPerformed = false;
                }
            }

            textResult.getEditText().setText(displayText.displayInsertedValue(sb, textResult.getEditText().getText().toString(), b.getText().toString()));
            newValue = new BigDecimal(sb.toString().replace(",", "")).stripTrailingZeros();
            if (!sb.toString().contains(".")) {
                textResult.getEditText().setText(displayText.addCommaToValue(sb, newValue));
            }

            if (!sb.toString().equals("0")) {
                if (!clearActivated) {
                    activateClearButton();
                }
            }

            if (numberSelected) {
                stackOperands.pop();
            }

            numberSelected = true;
            stackOperands.push(newValue);

            if (newValue.precision() == 15) {
                maxReached = true;
            }

            newValue = null;


            if (buttonOperationPerformed != null) {
                changeButtonAppearance(buttonOperationPerformed, getResources().getColor(R.color.md_white_1000),
                        getResources().getColor(R.color.md_amber_800));
                buttonOperationPerformed = null;
            }

            if (buttonScientificOperationPerformed != null) {
                changeButtonAppearance(buttonScientificOperationPerformed, getResources().getColor(R.color.md_white_1000),
                        getResources().getColor(R.color.md_grey_900));
                buttonScientificOperationPerformed = null;
            }
        };

        View.OnClickListener dotListener = v -> {
            Button b = (Button) v;
            if (constantPerformed || maxReached) {
                return;
            }
            if ((operationPerformed && !negativePerformed) || (memoryPerformed && !negativePerformed)) {
                if (pendingOperation.equals("=")) {
                    stackOperands.clear();
                    numberSelected = false;
                    result = null;
                }
                textResult.getEditText().setText("0");
            } else if (pendingOperation.equals("=") && !numberSelected) {
                stackOperands.clear();
                numberSelected = false;
                result = null;
                textResult.getEditText().setText("0");
                if (negativePerformed) {
                    negativePerformed = false;
                }
                if (percentPerformed) {
                    percentPerformed = false;
                }
            }
            if (!textResult.getEditText().getText().toString().contains(".")) {
                textResult.getEditText().append(b.getText().toString());
                if (!clearActivated) {
                    activateClearButton();
                }
                dotPerformed = true;
            }
        };

        View.OnClickListener operationListener = v -> {
            operationPerformed = true;
            constantPerformed = false;
            maxReached = false;
            Button b = (Button) v;
            String operation = b.getText().toString();

            try {
                if (stackOperands.isEmpty()) {
                    stackOperands.push(BigDecimal.ZERO);
                    if (result == null) {
                        result = stackOperands.peek();
                    }
                }
                if (numberSelected || dotPerformed) {
                    performStandardOperation(operation);
                } else if (stackOperation.size() >= 1 && stackOperands.size() > 1) {
                    if (!operation.equals("=")) {
                        stackOperation.pop();
                    }
                    performStandardOperation(operation);
                }
            } catch (Exception e) {
                e.printStackTrace();
                textResult.getEditText().setText(R.string.error);
                return;
            }

            if (!operation.equals("=")) {
                stackOperation.push(operation);
            }
            pendingOperation = operation;

//            if (negativePerformed) {
//                negativePerformed = false;
//            }

            if (buttonOperationPerformed != null) {
                changeButtonAppearance(buttonOperationPerformed, getResources().getColor(R.color.md_white_1000),
                        getResources().getColor(R.color.md_amber_800));
                buttonOperationPerformed = null;
            }

            if (buttonScientificOperationPerformed != null) {
                changeButtonAppearance(buttonScientificOperationPerformed, getResources().getColor(R.color.md_white_1000),
                        getResources().getColor(R.color.md_grey_900));
                buttonScientificOperationPerformed = null;
            }

            if (!operation.equals("=")) {
                if (operation.contains("x") || operation.contains("y") || operation.equals("EE") || operation.contains("log")) {
                    buttonScientificOperationPerformed = b;
                    changeButtonAppearance(buttonScientificOperationPerformed, getResources().getColor(R.color.md_grey_900),
                            getResources().getColor(R.color.md_white_1000));

                } else {
                    buttonOperationPerformed = b;
                    changeButtonAppearance(buttonOperationPerformed, getResources().getColor(R.color.md_amber_800),
                            getResources().getColor(R.color.md_white_1000));
                }
            }

        };

        View.OnClickListener percentListener = v -> {
            constantPerformed = false;
            maxReached = false;
            try {
                if (!stackOperands.isEmpty()) {
                    percent = stackOperands.pop().divide(BigDecimal.valueOf(100), MathContext.DECIMAL64);
                    stackOperands.push(percent);

                    if (result == null || stackOperands.size() == 1) {
                        result = percent;
                    }

                    if (textResult.getEditText() != null) {
                        textResult.getEditText().setText(displayText.addCommaToValue(sb, percent));
                    }

                    percent = null;
                    numberSelected = false;
                    firstTime = true;
                } else {
                    textResult.getEditText().setText("0");
                }

                if (dotPerformed) {
                    dotPerformed = false;
                }

                percentPerformed = true;
            } catch (Exception e) {
                e.printStackTrace();
                textResult.getEditText().setText(R.string.error);
            }
        };

        View.OnClickListener negativeListener = v -> {
            negativePerformed = true;
            String value = textResult.getEditText().getText().toString();

            if (operationPerformed && !constantPerformed) {
                if (!pendingOperation.equals("=") && !pendingOperation.equals("percent") && firstTime) {
                    textResult.getEditText().setText("0");
                    if (sb.length() != 0) {
                        sb.delete(0, sb.length());
                    }
                    value = textResult.getEditText().getText().toString();
                    firstTime = false;
                    if (dotPerformed) {
                        textResult.getEditText().getText().append(".");
                    }
                }
            }

            if (value.contains("-")) {
                textResult.getEditText().setText(value.replace("-", ""));
            } else {
                textResult.getEditText().getText().insert(0, "-");
            }

            if ((stackOperands.size() > 0 && !operationPerformed) || constantPerformed) {
                if (operationPerformed && !pendingOperation.equals("=")) {
                    if (stackOperands.peek().doubleValue() != 0) {
                        stackOperands.push(BigDecimal.ZERO);
                    }
                    numberSelected = true;
                }
                negative = stackOperands.pop();
                try {
                    negative = negative.multiply(BigDecimal.valueOf(-1), MathContext.DECIMAL64);
                    if (result == null) {
                        result = negative;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    textResult.getEditText().setText(R.string.error);
                }
                stackOperands.push(negative);
                negative = null;
            } else if(stackOperands.size() > 0) {
                if (stackOperands.peek().doubleValue() != Double.parseDouble(textResult.getEditText().getText().toString())) {
                    negative = stackOperands.pop();
                    negative = negative.multiply(BigDecimal.valueOf(-1), MathContext.DECIMAL64);
                    stackOperands.push(negative);
                }
            }

        };

        View.OnClickListener clearListener = v -> {
            if (sb.length() != 0) {
                sb.delete(0, sb.length());
            }
            if (!stackOperands.isEmpty()) {
                stackOperands.pop();
            }
            if (memoryPerformed) {
                memoryPerformed = false;
            }
            if (percentPerformed) {
                percentPerformed = false;
            }
            if (functionPerformed) {
                functionPerformed = false;
            }
            if (numberSelected) {
                numberSelected = false;
            }
            if (negativePerformed) {
                negativePerformed = false;
            }
            if (constantPerformed) {
                constantPerformed = false;
            }
            if (maxReached) {
                maxReached = false;
            }
            if (dotPerformed) {
                dotPerformed = false;
            }
            if (clearActivated) {
                clearActivated = false;
            }
            textResult.getEditText().setText("0");
            textResult.getEditText().setTextSize(TypedValue.COMPLEX_UNIT_SP, textSizePixel / scaledDensity);
            buttonClear.setVisibility(View.GONE);
            buttonAllClear.setVisibility(View.VISIBLE);
        };

        View.OnClickListener allClearListener = v -> {
            textResult.getEditText().setText("0");
            textResult.getEditText().setTextSize(TypedValue.COMPLEX_UNIT_SP, textSizePixel / scaledDensity);
            if (sb.length() != 0) {
                sb.delete(0, sb.length());
            }
            numberSelected = false;
            operationPerformed = false;
            functionPerformed = false;
            percentPerformed = false;
            negativePerformed = false;
            constantPerformed = false;
            maxReached = false;
            memoryPerformed = false;
            dotPerformed = false;
            clearActivated = false;
            firstTime = true;
            result = null;
            pendingOperation = "=";
            lastOperation = null;
            stackOperands.clear();
            stackOperation.clear();
            firstOperand = null;
            secondOperand = null;
            if (buttonOperationPerformed != null) {
                changeButtonAppearance(buttonOperationPerformed, getResources().getColor(R.color.md_white_1000),
                        getResources().getColor(R.color.md_amber_800));
                buttonOperationPerformed = null;
            }
            if (buttonScientificOperationPerformed != null) {
                changeButtonAppearance(buttonScientificOperationPerformed, getResources().getColor(R.color.md_white_1000),
                        getResources().getColor(R.color.md_grey_900));
                buttonScientificOperationPerformed = null;
            }
        };

        View.OnClickListener buttonSecondListener = view -> {
            if (buttonSecondPerformed) {
                activateSecondFunctionSet(View.GONE);
                activateFirstFunctionSet(View.VISIBLE);
                changeButtonAppearance(buttonSecond, getResources().getColor(R.color.md_white_1000),
                        getResources().getColor(R.color.md_grey_900));
                buttonSecondPerformed = false;
            } else {
                activateFirstFunctionSet(View.GONE);
                activateSecondFunctionSet(View.VISIBLE);
                changeButtonAppearance(buttonSecond, getResources().getColor(R.color.md_grey_900),
                        getResources().getColor(R.color.md_white_1000));
                buttonSecondPerformed = true;
            }
        };

        View.OnClickListener scientificFunctionsListener = view -> {
            functionPerformed = true;
            constantPerformed = false;
            maxReached = false;
            Button b = (Button) view;
            String function = b.getText().toString();
            try {
                performFunction(function);
            } catch (Exception e) {
                e.printStackTrace();
                textResult.getEditText().setText(R.string.error);
            }
        };

        View.OnClickListener constantListener = view -> {
            constantPerformed = true;
            maxReached = false;
            Button b = (Button) view;
            String constant = b.getText().toString();

            if (!stackOperands.isEmpty()) {
                if (operationPerformed) {
                    if (negativePerformed) {
                        stackOperands.pop();
                    }
                    result = BigDecimal.ZERO;
                } else {
                    result = stackOperands.pop();
                }
            } else {
                result = BigDecimal.ZERO;
            }

            try {
                switch (constant) {
                    case "Rand":
                        result = scientificFunctions.functionRandom();
                        break;
                    case "e":
                        result = new BigDecimal(Math.E, MathContext.DECIMAL64);
                        break;
                    case "\uD835\uDF7F":
                        result = new BigDecimal(Math.PI, MathContext.DECIMAL64);
                        break;
                }

                if (!clearActivated) {
                    activateClearButton();
                }

                if (negativePerformed) {
                    negativePerformed = false;
                }

                if (numberSelected) {
                    numberSelected = false;
                }
                if (dotPerformed) {
                    dotPerformed = false;
                }

                stackOperands.push(result);

                if (textResult.getEditText() != null) {
                    textResult.getEditText().setText(displayText.addCommaToValue(sb, result));
                }
            } catch (Exception e) {
                e.printStackTrace();
                textResult.getEditText().setText(R.string.error);
            }
        };

        View.OnClickListener radiansListener = view -> {
            buttonRadians.setVisibility(View.GONE);
            buttonDegrees.setVisibility(View.VISIBLE);
            textRadian.setVisibility(View.VISIBLE);
            radiansActivated = true;
        };

        View.OnClickListener degreesListener = view -> {
            buttonRadians.setVisibility(View.VISIBLE);
            buttonDegrees.setVisibility(View.GONE);
            textRadian.setVisibility(View.GONE);
            radiansActivated = false;
        };

        View.OnClickListener memoryListener = view -> {
            memoryPerformed = true;
            constantPerformed = false;
            maxReached = false;
            Button b = (Button) view;
            try {
                performMemoryOperation(b.getText().toString());
            } catch (Exception e) {
                e.printStackTrace();
                textResult.getEditText().setText(R.string.error);
            }
        };

        View.OnClickListener bracketListener = view -> {
            constantPerformed = false;
            maxReached = false;
            Button b = (Button) view;
            switch (b.getText().toString()) {
                case "(":
                    stackOperation.push("(");
                    result = null;
                    break;
                case ")":
                    if (!stackOperation.isEmpty() && stackOperation.contains("(")) {
                        performStandardOperation(")");
                    }
                    break;
            }
        };

        button0.setOnClickListener(numberListener);
        button1.setOnClickListener(numberListener);
        button2.setOnClickListener(numberListener);
        button3.setOnClickListener(numberListener);
        button4.setOnClickListener(numberListener);
        button5.setOnClickListener(numberListener);
        button6.setOnClickListener(numberListener);
        button7.setOnClickListener(numberListener);
        button8.setOnClickListener(numberListener);
        button9.setOnClickListener(numberListener);

        buttonEquals.setOnClickListener(operationListener);
        buttonDivide.setOnClickListener(operationListener);
        buttonMultiply.setOnClickListener(operationListener);
        buttonMinus.setOnClickListener(operationListener);
        buttonPlus.setOnClickListener(operationListener);

        buttonDot.setOnClickListener(dotListener);
        buttonPercent.setOnClickListener(percentListener);
        buttonNegative.setOnClickListener(negativeListener);

        buttonClear.setOnClickListener(clearListener);
        buttonAllClear.setOnClickListener(allClearListener);

        if (scientificMode) {
            buttonSecond.setOnClickListener(buttonSecondListener);

            buttonLogBaseY.setOnClickListener(operationListener);
            buttonYPowerX.setOnClickListener(operationListener);
            buttonXPowerY.setOnClickListener(operationListener);
            buttonXRootY.setOnClickListener(operationListener);
            buttonEngineeringExponent.setOnClickListener(operationListener);

            buttonRadians.setOnClickListener(radiansListener);
            buttonDegrees.setOnClickListener(degreesListener);

            buttonEPowerX.setOnClickListener(scientificFunctionsListener);
            button10PowerX.setOnClickListener(scientificFunctionsListener);
            button2PowerX.setOnClickListener(scientificFunctionsListener);
            buttonNaturalLog.setOnClickListener(scientificFunctionsListener);
            buttonLogBase10.setOnClickListener(scientificFunctionsListener);
            buttonLogBase2.setOnClickListener(scientificFunctionsListener);

            buttonArcSine.setOnClickListener(scientificFunctionsListener);
            buttonArcCosine.setOnClickListener(scientificFunctionsListener);
            buttonArcTangent.setOnClickListener(scientificFunctionsListener);
            buttonSine.setOnClickListener(scientificFunctionsListener);
            buttonCosine.setOnClickListener(scientificFunctionsListener);
            buttonTangent.setOnClickListener(scientificFunctionsListener);

            buttonInverseHyperbolicSine.setOnClickListener(scientificFunctionsListener);
            buttonInverseHyperbolicCosine.setOnClickListener(scientificFunctionsListener);
            buttonInverseHyperbolicTangent.setOnClickListener(scientificFunctionsListener);
            buttonHyperbolicSine.setOnClickListener(scientificFunctionsListener);
            buttonHyperbolicCosine.setOnClickListener(scientificFunctionsListener);
            buttonHyperbolicTangent.setOnClickListener(scientificFunctionsListener);

            buttonSquare.setOnClickListener(scientificFunctionsListener);
            buttonCube.setOnClickListener(scientificFunctionsListener);
            buttonReciprocalValue.setOnClickListener(scientificFunctionsListener);
            buttonSquareRoot.setOnClickListener(scientificFunctionsListener);
            buttonCubeRoot.setOnClickListener(scientificFunctionsListener);

            buttonFactorial.setOnClickListener(scientificFunctionsListener);

            buttonRandom.setOnClickListener(constantListener);
            buttonEulerNumber.setOnClickListener(constantListener);
            buttonPi.setOnClickListener(constantListener);

            buttonMemoryClear.setOnClickListener(memoryListener);
            buttonMemoryPlus.setOnClickListener(memoryListener);
            buttonMemoryMinus.setOnClickListener(memoryListener);
            buttonMemoryRecall.setOnClickListener(memoryListener);

            buttonOpenBracket.setOnClickListener(bracketListener);
            buttonClosedBracket.setOnClickListener(bracketListener);
        }

    }

    private void initializeOperationPriorities() {
        operationPriority.put("(", 4);
        operationPriority.put(")", 4);
        operationPriority.put("xʸ", 3);
        operationPriority.put("yˣ", 3);
        operationPriority.put("EE", 3);
        operationPriority.put("ʸ√x", 3);
        operationPriority.put("logᵧ", 3);
        operationPriority.put("÷", 2);
        operationPriority.put("×", 2);
        operationPriority.put("+", 1);
        operationPriority.put("-", 1);
        operationPriority.put("=", 0);
    }

    private void activateFirstFunctionSet(int visibility) {
        buttonEPowerX.setVisibility(visibility);
        button10PowerX.setVisibility(visibility);
        buttonNaturalLog.setVisibility(visibility);
        buttonLogBase10.setVisibility(visibility);
        buttonSine.setVisibility(visibility);
        buttonCosine.setVisibility(visibility);
        buttonTangent.setVisibility(visibility);
        buttonHyperbolicSine.setVisibility(visibility);
        buttonHyperbolicCosine.setVisibility(visibility);
        buttonHyperbolicTangent.setVisibility(visibility);
    }

    private void activateSecondFunctionSet(int visibility) {
        buttonYPowerX.setVisibility(visibility);
        button2PowerX.setVisibility(visibility);
        buttonLogBaseY.setVisibility(visibility);
        buttonLogBase2.setVisibility(visibility);
        buttonArcSine.setVisibility(visibility);
        buttonArcCosine.setVisibility(visibility);
        buttonArcTangent.setVisibility(visibility);
        buttonInverseHyperbolicSine.setVisibility(visibility);
        buttonInverseHyperbolicCosine.setVisibility(visibility);
        buttonInverseHyperbolicTangent.setVisibility(visibility);
    }

    private void activateClearButton() {
        buttonAllClear.setVisibility(View.GONE);
        buttonClear.setVisibility(View.VISIBLE);
        clearActivated = true;
    }

    private void initializeStandardButtons() {
        textResult = findViewById(R.id.text_input_result);

        button0 = findViewById(R.id.button_zero);
        button1 = findViewById(R.id.button_one);
        button2 = findViewById(R.id.button_two);
        button3 = findViewById(R.id.button_three);
        button4 = findViewById(R.id.button_four);
        button5 = findViewById(R.id.button_five);
        button6 = findViewById(R.id.button_six);
        button7 = findViewById(R.id.button_seven);
        button8 = findViewById(R.id.button_eight);
        button9 = findViewById(R.id.button_nine);
        buttonDot = findViewById(R.id.button_dot);

        buttonEquals = findViewById(R.id.button_equals);
        buttonPlus = findViewById(R.id.button_plus);
        buttonMinus = findViewById(R.id.button_minus);
        buttonMultiply = findViewById(R.id.button_multiply);
        buttonDivide = findViewById(R.id.button_divide);

        buttonPercent = findViewById(R.id.button_percent);
        buttonNegative = findViewById(R.id.button_negative);
        buttonClear = findViewById(R.id.button_clear);
        buttonAllClear = findViewById(R.id.button_all_clear);
    }

    private void initializeScientificButtons() {
        buttonSecond = findViewById(R.id.button_second);

        buttonLogBaseY = findViewById(R.id.button_log_base_y);
        buttonYPowerX = findViewById(R.id.button_y_power_x);
        buttonXPowerY = findViewById(R.id.button_x_power_y);
        buttonXRootY = findViewById(R.id.button_x_root_y);
        buttonEngineeringExponent = findViewById(R.id.button_engineering_exponent);

        textRadian = findViewById(R.id.text_view_radian);
        buttonRadians = findViewById(R.id.button_radians);
        buttonDegrees = findViewById(R.id.button_degrees);

        buttonEPowerX = findViewById(R.id.button_e_power_x);
        button10PowerX = findViewById(R.id.button_10_power_x);
        button2PowerX = findViewById(R.id.button_2_power_x);
        buttonNaturalLog = findViewById(R.id.button_natural_log);
        buttonLogBase10 = findViewById(R.id.button_log_base_10);
        buttonLogBase2 = findViewById(R.id.button_log_base_2);

        buttonArcSine = findViewById(R.id.button_arc_sine);
        buttonArcCosine = findViewById(R.id.button_arc_cosine);
        buttonArcTangent = findViewById(R.id.button_arc_tangent);
        buttonSine = findViewById(R.id.button_sine);
        buttonCosine = findViewById(R.id.button_cosine);
        buttonTangent = findViewById(R.id.button_tangent);

        buttonInverseHyperbolicSine = findViewById(R.id.button_inverse_hyperbolic_sine);
        buttonInverseHyperbolicCosine = findViewById(R.id.button_inverse_hyperbolic_cosine);
        buttonInverseHyperbolicTangent = findViewById(R.id.button_inverse_hyperbolic_tangent);
        buttonHyperbolicSine = findViewById(R.id.button_hyperbolic_sine);
        buttonHyperbolicCosine = findViewById(R.id.button_hyperbolic_cosine);
        buttonHyperbolicTangent = findViewById(R.id.button_hyperbolic_tangent);

        buttonSquare = findViewById(R.id.button_square);
        buttonCube = findViewById(R.id.button_cube);
        buttonReciprocalValue = findViewById(R.id.button_reciprocal_value);
        buttonSquareRoot = findViewById(R.id.button_square_root);
        buttonCubeRoot = findViewById(R.id.button_cube_root);
        buttonFactorial = findViewById(R.id.button_factorial);

        buttonMemoryClear = findViewById(R.id.button_memory_clear);
        buttonMemoryPlus = findViewById(R.id.button_memory_plus);
        buttonMemoryMinus = findViewById(R.id.button_memory_minus);
        buttonMemoryRecall = findViewById(R.id.button_memory_recall);

        buttonRandom = findViewById(R.id.button_random);
        buttonEulerNumber = findViewById(R.id.button_eulers_number);
        buttonPi = findViewById(R.id.button_pi);

        buttonOpenBracket = findViewById(R.id.button_open_bracket);
        buttonClosedBracket = findViewById(R.id.button_closed_bracket);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(STATE_BUTTON_SECOND_PERFORMED, buttonSecondPerformed);
        outState.putBoolean(STATE_NUMBER_SELECTED, numberSelected);
        outState.putBoolean(STATE_OPERATION_PERFORMED, operationPerformed);
        outState.putBoolean(STATE_FUNCTION_PERFORMED, functionPerformed);
        outState.putBoolean(STATE_PERCENT_PERFORMED, percentPerformed);
        outState.putBoolean(STATE_CONSTANT_PERFORMED, constantPerformed);
        outState.putBoolean(STATE_MAXIMUM_REACHED, maxReached);
        if (memoryValue != null) {
            outState.putDouble(STATE_MEMORY_VALUE, memoryValue.doubleValue());
        }
        outState.putBoolean(STATE_MEMORY_PERFORMED, memoryPerformed);
        outState.putBoolean(STATE_MEMORY_FIRST_TIME, firstTimeMemory);
        outState.putBoolean(STATE_MEMORY_CLEAR, memoryClear);
        outState.putBoolean(STATE_RADIANS_ACTIVATED, radiansActivated);
        outState.putStringArrayList("stackOperationArray", new ArrayList<>(stackOperation));
        outState.putDoubleArray("stackOperandsArray", stackOperands.stream().mapToDouble(BigDecimal::doubleValue).toArray());
        stackOperandsArray = null;
        stackOperationArray = null;
        outState.putBoolean(STATE_CLEAR_ACTIVATED, clearActivated);
        outState.putBoolean(STATE_NEGATIVE_PERFORMED, negativePerformed);
        outState.putBoolean(STATE_DOT_PERFORMED, dotPerformed);
        outState.putBoolean(STATE_FIRST_TIME, firstTime);
        if (sb != null) {
            outState.putString(STATE_STRING_BUILDER, sb.toString());
        }
        if (buttonOperationPerformed != null) {
            outState.putInt(STATE_BUTTON_OPERATION_PERFORMED, buttonOperationPerformed.getId());
        }
        if (buttonScientificOperationPerformed != null) {
            outState.putInt(STATE_BUTTON_SCIENTIFIC_OPERATION_PERFORMED, buttonScientificOperationPerformed.getId());
        }
        outState.putString(STATE_PENDING_OPERATION, pendingOperation);
        if (lastOperation != null) {
            outState.putString(STATE_LAST_OPERATION, lastOperation);
        }
        if (textResult.getEditText() != null) {
            outState.putString(STATE_TEXT_RESULT, textResult.getEditText().getText().toString());
        }
        if (result != null) {
            outState.putDouble(STATE_RESULT, result.doubleValue());
        }
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        buttonSecondPerformed = savedInstanceState.getBoolean(STATE_BUTTON_SECOND_PERFORMED);
        if (buttonSecondPerformed && buttonSecond != null) {
            activateFirstFunctionSet(View.GONE);
            activateSecondFunctionSet(View.VISIBLE);
            changeButtonAppearance(buttonSecond, getResources().getColor(R.color.md_grey_900),
                    getResources().getColor(R.color.md_white_1000));
        }
        stackOperationArray = savedInstanceState.getStringArrayList("stackOperationArray");
        stackOperandsArray = savedInstanceState.getDoubleArray("stackOperandsArray");

        if (stackOperandsArray != null) {
            for (double v : stackOperandsArray) {
                stackOperands.push(BigDecimal.valueOf(v));
            }
        }
        if (stackOperationArray != null) {
            for (String s : stackOperationArray) {
                stackOperation.push(s);
            }
        }

        memoryPerformed = savedInstanceState.getBoolean(STATE_MEMORY_PERFORMED);
        memoryClear = savedInstanceState.getBoolean(STATE_MEMORY_CLEAR);
        firstTimeMemory = savedInstanceState.getBoolean(STATE_MEMORY_FIRST_TIME);
        memoryValue = BigDecimal.valueOf(savedInstanceState.getDouble(STATE_MEMORY_VALUE));

        radiansActivated = savedInstanceState.getBoolean(STATE_RADIANS_ACTIVATED);
        if (radiansActivated && buttonRadians != null && buttonDegrees != null && textRadian != null) {
            buttonRadians.setVisibility(View.GONE);
            buttonDegrees.setVisibility(View.VISIBLE);
            textRadian.setVisibility(View.VISIBLE);
        }

        dotPerformed = savedInstanceState.getBoolean(STATE_DOT_PERFORMED);
        percentPerformed = savedInstanceState.getBoolean(STATE_PERCENT_PERFORMED);
        negativePerformed = savedInstanceState.getBoolean(STATE_NEGATIVE_PERFORMED);

        numberSelected = savedInstanceState.getBoolean(STATE_NUMBER_SELECTED);
        clearActivated = savedInstanceState.getBoolean(STATE_CLEAR_ACTIVATED);
        if (clearActivated) {
            activateClearButton();
        }
        constantPerformed = savedInstanceState.getBoolean(STATE_CONSTANT_PERFORMED);
        maxReached = savedInstanceState.getBoolean(STATE_MAXIMUM_REACHED);

        operationPerformed = savedInstanceState.getBoolean(STATE_OPERATION_PERFORMED);
        functionPerformed = savedInstanceState.getBoolean(STATE_FUNCTION_PERFORMED);
        pendingOperation = savedInstanceState.getString(STATE_PENDING_OPERATION);
        lastOperation = savedInstanceState.getString(STATE_LAST_OPERATION);
        firstTime = savedInstanceState.getBoolean(STATE_FIRST_TIME);

        if (savedInstanceState.getDouble(STATE_RESULT) != 0) {
            result = BigDecimal.valueOf(savedInstanceState.getDouble(STATE_RESULT));
        }

        buttonOperationPerformed = findViewById(savedInstanceState.getInt(STATE_BUTTON_OPERATION_PERFORMED));
        buttonScientificOperationPerformed = findViewById(savedInstanceState.getInt(STATE_BUTTON_SCIENTIFIC_OPERATION_PERFORMED));

        if (savedInstanceState.getString(STATE_STRING_BUILDER) != null) {
            sb.append(savedInstanceState.getString(STATE_STRING_BUILDER));
        }

        if (textResult.getEditText() != null) {
            textResult.getEditText().setText(savedInstanceState.getString(STATE_TEXT_RESULT));
            if (!scientificMode) {
                int s = textResult.getEditText().getText().toString()
                        .replace(",", "")
                        .replace(".", "")
                        .length();
                if (s < 10) {
                    textResult.getEditText().setTextSize(TypedValue.COMPLEX_UNIT_SP, textSizePixel / scaledDensity);
                } else if (s < 13) {
                    textResult.getEditText().setTextSize(TypedValue.COMPLEX_UNIT_SP, textSizePixel / scaledDensity - 12);
                } else {
                    textResult.getEditText().setTextSize(TypedValue.COMPLEX_UNIT_SP, textSizePixel / scaledDensity - 20);
                }
            }
        }

        if (buttonOperationPerformed != null) {
            changeButtonAppearance(buttonOperationPerformed, getResources().getColor(R.color.md_amber_800),
                    getResources().getColor(R.color.md_white_1000));
        }
        if (buttonScientificOperationPerformed != null) {
            changeButtonAppearance(buttonScientificOperationPerformed, getResources().getColor(R.color.md_grey_900),
                    getResources().getColor(R.color.md_white_1000));
        }
    }

    private void performStandardOperation(String operation) {
        if (result == null) {
            result = stackOperands.peek();
        } else {
            if (lastOperation == null) {
                lastOperation = pendingOperation;
            }

            if (operationPriority.get(operation) != null && operationPriority.get(lastOperation) != null) {
                currentOperationPriority = operationPriority.get(operation);
                lastOperationPriority = operationPriority.get(lastOperation);
            }

            if (currentOperationPriority != null && lastOperationPriority != null &&
                    currentOperationPriority > lastOperationPriority && !operation.equals(")")) {

                if (!operation.equals(pendingOperation) && !stackOperation.isEmpty()) {
                    lastOperation = stackOperation.peek();
                }

                firstTime = true;
                numberSelected = false;
                dotPerformed = false;
                return;
            } else {
                lastOperation = null;

                while (stackOperands.size() > 1 && !stackOperation.isEmpty()) {
                    pendingOperation = stackOperation.pop();

                    if (pendingOperation.equals("=")) {
                        pendingOperation = operation;
                    }

                    if (pendingOperation.equals("(")) {
                        if (operation.equals(")")) {
                            break;
                        } else if (operation.equals("=")) {
                            continue;
                        } else {
                            stackOperation.push("(");
                            break;
                        }
                    }

                    secondOperand = stackOperands.pop();
                    firstOperand = stackOperands.pop();

                    switch (pendingOperation) {
                        case "=":
                            result = stackOperands.pop();
                            break;
                        case "÷":
                            result = firstOperand.divide(secondOperand, MathContext.DECIMAL64);
                            break;
                        case "×":
                            result = firstOperand.multiply(secondOperand, MathContext.DECIMAL64);
                            break;
                        case "+":
                            result = firstOperand.add(secondOperand, MathContext.DECIMAL64);
                            break;
                        case "-":
                            result = firstOperand.subtract(secondOperand, MathContext.DECIMAL64);
                            break;
                        case "xʸ":
                            result = scientificOperations.operationXPowerOfY(firstOperand, secondOperand);
                            break;
                        case "yˣ":
                            result = scientificOperations.operationYPowerOfX(firstOperand, secondOperand);
                            break;
                        case "ʸ√x":
                            result = scientificOperations.operationYRootOfX(firstOperand, secondOperand);
                            break;
                        case "logᵧ":
                            result = scientificOperations.operationLogarithmBaseY(firstOperand, secondOperand);
                            break;
                        case "EE":
                            result = scientificOperations.operationScaleByPowerOf10(firstOperand, secondOperand);
                            break;
                    }

                    firstOperand = null;
                    secondOperand = null;
                    stackOperands.push(result);
                }
            }
        }

        if (textResult.getEditText() != null) {
            if (result.compareTo(new BigDecimal(1).scaleByPowerOfTen(15)) >= 0) {
                textResult.getEditText().setText(displayText.scientificNotation(sb, result));
            } else {
                textResult.getEditText().setText(displayText.addCommaToValue(sb, result));
            }
        }

        firstTime = true;
        numberSelected = false;
        dotPerformed = false;
    }

    private void performMemoryOperation(String operation) {
        switch (operation) {
            case "mc":
                if (!memoryClear) {
                    memoryValue = BigDecimal.ZERO;
                    if (buttonMemoryRecall != null) {
                        changeButtonAppearance(buttonMemoryRecall, getResources().getColor(R.color.md_white_1000),
                                getResources().getColor(R.color.md_grey_900));
                    }
                    memoryClear = true;
                    firstTimeMemory = true;
                }
                break;
            case "m+":
            case "m-":
                if (stackOperands.isEmpty()) {
                    stackOperands.push(BigDecimal.ZERO);
                }
                memoryValue = operation.equals("m+") ? memoryValue.add(stackOperands.peek(), MathContext.DECIMAL64) : memoryValue.subtract(stackOperands.peek(), MathContext.DECIMAL64);
                memoryClear = false;
                break;
            case "mr":
                numberSelected = true;
                stackOperands.push(memoryValue);

                if (textResult.getEditText() != null) {
                    if (memoryValue.compareTo(new BigDecimal(1).scaleByPowerOfTen(15)) >= 0) {
                        textResult.getEditText().setText(displayText.scientificNotation(sb, memoryValue));
                    } else {
                        textResult.getEditText().setText(displayText.addCommaToValue(sb, memoryValue));
                    }
                }
                break;
        }
        if (!memoryClear && firstTimeMemory) {
            changeButtonAppearance(buttonMemoryRecall, getResources().getColor(R.color.md_grey_900),
                    getResources().getColor(R.color.md_white_1000));
            firstTimeMemory = false;
        }
    }

    public void performFunction(String function) {
        if (stackOperands.isEmpty()) {
            stackOperands.push(BigDecimal.ZERO);
        }

        result = stackOperands.peek();

        switch (function) {
            case "x²":
                result = scientificFunctions.functionSquare(stackOperands.pop());
                break;
            case "x³":
                result = scientificFunctions.functionCube(stackOperands.pop());
                break;
            case "eˣ":
                result = scientificFunctions.functionEPowerX(stackOperands.pop());
                break;
            case "10ˣ":
                result = scientificFunctions.function10PowerX(stackOperands.pop());
                break;
            case "2ˣ":
                result = scientificFunctions.function2PowerX(stackOperands.pop());
                break;
            case "¹/ₓ":
                result = scientificFunctions.functionReciprocialValue(stackOperands.pop());
                break;
            case "√x":
                result = scientificFunctions.functionSquareRoot(stackOperands.pop());
                break;
            case "∛x":
                result = scientificFunctions.functionCubeRoot(stackOperands.pop());
                break;
            case "ln":
                result = scientificFunctions.functionNaturalLogarithm(stackOperands.pop());
                break;
            case "log₁₀":
                result = scientificFunctions.functionLogarithmBase10(stackOperands.pop());
                break;
            case "log₂":
                result = scientificFunctions.functionLogarithmBase2(stackOperands.pop());
                break;
            case "x!":
                result = scientificFunctions.functionFactorial(stackOperands.pop());
                break;
            case "sin":
                result = scientificFunctions.functionSine(stackOperands.pop(), radiansActivated);
                break;
            case "cos":
                result = scientificFunctions.functionCosine(stackOperands.pop(), radiansActivated);
                break;
            case "tan":
                result = scientificFunctions.functionTangent(stackOperands.pop(), radiansActivated);
                break;
            case "sin⁻¹":
                result = scientificFunctions.functionArcSine(stackOperands.pop(), radiansActivated);
                break;
            case "cos⁻¹":
                result = scientificFunctions.functionArcCosine(stackOperands.pop(), radiansActivated);
                break;
            case "tan⁻¹":
                result = scientificFunctions.functionArcTangent(stackOperands.pop(), radiansActivated);
                break;
            case "sinh":
                result = scientificFunctions.functionHyperbolicSine(stackOperands.pop());
                break;
            case "cosh":
                result = scientificFunctions.functionHyperbolicCosine(stackOperands.pop());
                break;
            case "tanh":
                result = scientificFunctions.functionHyperbolicTangent(stackOperands.pop());
                break;
            case "sinh⁻¹":
                result = scientificFunctions.functionInverseHyperbolicSine(stackOperands.pop());
                break;
            case "cosh⁻¹":
                result = scientificFunctions.functionInverseHyperbolicCosine(stackOperands.pop());
                break;
            case "tanh⁻¹":
                result = scientificFunctions.functionInverseHyperbolicTangent(stackOperands.pop());
                break;
        }

        if (numberSelected) {
            numberSelected = false;
        }
        if (negativePerformed) {
            negativePerformed = false;
        }
        if (dotPerformed) {
            dotPerformed = false;
        }

        stackOperands.push(result);

        if (textResult.getEditText() != null) {
            if (result.compareTo(new BigDecimal(1).scaleByPowerOfTen(15)) >= 0) {
                textResult.getEditText().setText(displayText.scientificNotation(sb, result));
            } else {
                textResult.getEditText().setText(displayText.addCommaToValue(sb, result));
            }
        }

    }

    private void changeButtonAppearance(final Button button, int text, int background) {
        ValueAnimator backgroundColorAnimator = ValueAnimator.ofObject(new ArgbEvaluator(), text, background);
        backgroundColorAnimator.setDuration(300);
        backgroundColorAnimator.addUpdateListener(animator -> button.setBackgroundColor((int) animator.getAnimatedValue()));

        ValueAnimator textColorAnimator = ValueAnimator.ofObject(new ArgbEvaluator(), background, text);
        textColorAnimator.setDuration(300);
        textColorAnimator.addUpdateListener(animator -> button.setTextColor((int) animator.getAnimatedValue()));

        textColorAnimator.start();
        backgroundColorAnimator.start();
    }

}
