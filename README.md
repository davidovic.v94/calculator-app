# CalculatorApp
Android application which can be used for simple and complex calculations.

## Table of contents
* [General info](#general-info)
* [Screenshots](#screenshots)
* [Technologies](#technologies)
* [Setup](#setup)
* [Status](#status)
* [Inspiration](#inspiration)

## General info
CalculatorApp works on the stack principle, where operands and operations are pushed on two separate stacks. Application follows the "PEMDAS" rule, so every operation has it's priority. This phrase ("PEMDAS") stands for "Parentheses, Exponents, Multiplication and Division, and Addition and Subtraction". This listing tells us the ranks of the operations: Parentheses outrank exponents, which outrank multiplication and division (but multiplication and division are at the same rank), and multiplication and division outrank addition and subtraction (which are together on the bottom rank). In other words, the precedence is:

1. Parentheses
2. Exponents
3. Multiplication and Division
4. Addition and Subtraction

The design of the app is reminiscent of an iOS calculator app.

You can choose between standard (portrait) and scientific (landscape) view.

## Screenshots

<img src="./images/Screenshot_20210120_140030.png" width="100">
<img src="./images/Screenshot_20210120_140048.png" width="100">
<img src="./images/Screenshot_20210120_140132.png" width="200">
<img src="./images/Screenshot_20210120_140139.png" width="200">
<img src="./images/Screenshot_20210120_140149.png" width="200">

## Technologies
Project is created with:

* Java 1.8
* Gradle 6.2.2

## Setup
To run this project, download and start it in android studio.

## Status
The project was made for learning purposes. It's not tested on real android device.

## Inspiration
I was inspired by iOS calculator app, so I tried to make my own app for Android.
